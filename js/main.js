$(document).ready(function() {
    
                /* Аякс - загрузка фото */
                $('body').on('change', '#userfile', function() {
                    // запуск анимации загрузки
                    $('#inima').prepend('<div class="ajax_animation"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></div>');
                    var data = new FormData();
                    data.append("userfile", this.files[0]);
                                        
                    $.ajax({
                        url: "/index.php/admin/do_upload",
                        type: "POST",
                        data: data,
                        processData: false,
                        contentType: false,
                        success: function(json)
                        {
                            data = jQuery.parseJSON(json);                                     
                            //console.log(data);
                            //очищаем содержимое блока от старого изображения и помещаем новое
                            $('#img_holst').empty();
                            $('#img_holst').append('<img src="/images/origin/' + data.file_name + '" id="target" alt="[Jcrop Example]" />');
                            //показать форму кропа после выбора файла
                            $("form#form_crop").css({"display":"block"});
                            //имя загруженного файла в поле формы кропа
                            $('input#path_file').val(data.file_name);
                            
                            /* start jcrop */
                            var jcrop_api;
                        
                            $('#target').Jcrop({
                              onChange:   showCoords,
                              onSelect:   showCoords,
                              onRelease:  clearCoords,
                              setSelect:   [ 88, 88, 225, 225 ],                              
                              minSize: [ 225, 225 ]                              
                            },function(){
                              jcrop_api = this;
                            });
    
                          // Simple event handler, called from onChange and onSelect
                          // event handlers, as per the Jcrop invocation above
                          function showCoords(c)
                          {
                            $('#x1').val(c.x);
                            $('#y1').val(c.y);
                            $('#x2').val(c.x2);
                            $('#y2').val(c.y2);
                            $('#w').val(c.w);
                            $('#h').val(c.h);
                          };
                        
                          function clearCoords()
                          {
                            $('#coords input').val('');
                          };
                          /* finish jcrop */
                                               
                        }
                    });
                    
                    //$('#userfile').val(''); //очищаем инпут
                    $('#inima .ajax_animation').remove(); //удаляем анимацию загрузки
                });
                
                // Ajax CROP обрезка фото - передаем данные формы обработчика jCROP
                $('body').on('click', 'button#but_crop', function(event) {
                    event.preventDefault();
                    var x = $('input#x1').val();
                    var y = $('input#y1').val();
                    var w = $('input#w').val();
                    var h = $('input#h').val();
                    var path_file = $('input#path_file').val();
                    //alert (path_file);
                    jQuery.ajax({
                        type: "POST",
                        url: "/index.php/admin/crop_img",
                        dataType: 'json',
                        cache: false,
                        data: {x: x, y: y, w: w, h: h, path_file: path_file},
                        error:function(){
                             alert("error");
                        },
                        success: function(json){
                            //console.log(json);
                            //очищаем содержимое блока от старого изображения и помещаем новое
                            $('#img_holst').empty();
                            //скрываем форму кропа
                            $("form#form_crop").css({"display":"none"});
                            //показываем обрезанную картинку c кнопкой удаления картинки из папки
                            // data-id (блока) на кнопке удаления не указываем так как картинка ещё не внесена в базу данных
                            $('#img_holst').append('<div id="img_holst_inner"><a href="#" data-block_image="' + path_file + '" id="delete_img"><i class="fa fa-times-circle" aria-hidden="true"></i></a><img src="/images/crop/' + path_file + '"/></div>');
                            //загружаем имя файла в скрытое поле для дальнейшей записи в бд
                            $("input#b_image").val(path_file);
                            //очищаем инпут
                            $('#userfile').val("");
                            //добавим сообщение-информацию для пользователя
                            $('#img_holst').prepend('<div class="alert alert-warning" role="alert">Изображение загружено, для сохранения нажмите кнопку "Отправить" внизу окна.</div>');
                            }
                    });
                });
				
              //МОДАЛЬНОЕ ОКНО ДОБАВЛЕНИЯ блока формы обратной связи
              $('body').on('click','#email_form_block_add',function(){
                //открыть модальное окно id="email_form_block_add" с формой добавл/редакт блока
                $("#email_form_block_add").modal('show');

                //заносим данные атрибута action
                $("form#my_block").attr("action","/index.php/admin/add_block");
                
              });
                
              //МОДАЛЬНОЕ ОКНО ДОБАВЛЕНИЯ БЛОКА
              $('body').on('click','#block_add',function(){
                //очищаем содержимое #img_holst итп
                $('#img_holst').empty();
                $('input#userfile').val('');
                $('input#b_image').val('');
                $("form#form_crop").css({"display":"none"});
                
                //открыть модальное окно id="add_edit_block" с формой добавл/редакт блока
                $("#add_edit_block").modal('show');
                
                //инициализируем codemirror
                var textArea = document.getElementById('txt_block');
                var editor = CodeMirror.fromTextArea(textArea, {
                  lineNumbers: true,
                  theme: "base16-light",
                  mode: "text/html"
                });
                                
                //при закрытии модального окна
                $('#add_edit_block').on('hidden.bs.modal', function () {
                    //уничтожаем codemirror
                    editor.toTextArea();
                    //очистим textarea (что необязательно, всё равно перезапишется editor.setValue(block_text))
                    $('textarea#txt_block').val('');
                });
                
                //обработка селектов(текст или html)
                $('input[name=editor_type]').change(function() {
                    var e_type = $('input[name=editor_type]:checked').val();
                    //если выбираем редактирование кода_html, подключим к CodeMirror тему blackboard
                    if (e_type == '2') {
                        editor.setOption("theme", "base16-dark");
                        editor.setOption("mode", "text/html");
                     };
                     //если выбираем редактирование кода_markdown, подключим к CodeMirror тему default
                     if (e_type == '1') {         
                        editor.setOption("theme", "base16-light");
                        editor.setOption("mode", "text/html");
                     };
                    
                });

                //заносим данные атрибута action
                $("form#my_block").attr("action","/index.php/admin/add_block");
                
              });
              
              //МОДАЛЬНОЕ ОКНО ДОБАВЛЕНИЯ СТРОКИ
              $('body').on('click','#row_add',function(){
                $("#add_edit_row").modal('show');

                //заносим данные атрибута action
                $("form#my_row").attr("action","/index.php/admin/add_row");
                //покажем если #b_generation скрыт
                $("#b_generation").css({"display":"block"});
                
              });
                
              //МОДАЛЬНОЕ ОКНО РЕДАКТИРОВАНИЯ БЛОКА
              $('body').on('click','.block_edit',function(){
                //очищаем содержимое #img_holst итп
                $('#img_holst').empty();
                $('input#userfile').val('');
                $('input#b_image').val('');
                $("form#form_crop").css({"display":"none"});
                
                //открыть модальное окно id="editblock" с формой редакт блока
                $("#add_edit_block").modal('show');
                //кнопка вызова модального окна имеет атрибут data с данными - эти данные заносим в переменные
                var block_id = $(this).data('block_id');
                var block_title = $(this).data('block_title');
                var block_text = $(this).data('block_text');
                var block_image = $(this).data('block_image');
                var block_width = $(this).data('block_width');
                var r_id = $(this).data('r_id');
                var b_position = $(this).data('b_position');
				var attach_form_id = $(this).data('attach_form_id');

                //заносим данные в поля формы - для редактирования блока
                $("form#my_block").attr("action","/index.php/admin/edit_block");
                $("input#b_title").val(block_title);
                $("input#b_image").val(block_image);
                $("input#b_id").val(block_id);
                $("textarea#txt_block").text(block_text);
                $("select#b_width").val(block_width);
                $("select#id_row").val(r_id);
                $("select#b_position").val(b_position);
                $("select#attach_form_id").val(attach_form_id);
                
                //инициализируем codemirror
                var textArea = document.getElementById('txt_block');
                var editor = CodeMirror.fromTextArea(textArea, {
                  lineNumbers: true,
                  theme: "base16-light",
                  mode: "text/html"
                });
                
                //при закрытии модального окна
                $('#add_edit_block').on('hidden.bs.modal', function () {
                    //уничтожаем codemirror
                    editor.toTextArea();
                    //очистим textarea (что необязательно, всё равно перезапишется editor.setValue(block_text))
                    $('textarea#txt_block').val('');
                });
                
                //заносим данные в textarea CodeMirror
                editor.setValue(block_text);
                //editor.refresh
                $('#add_edit_block').on('shown.bs.modal', function() { editor.refresh() });
                
                //обработка селектов(текст или html)
                $('input[name=editor_type]').change(function() {
                    var e_type = $('input[name=editor_type]:checked').val();
                    //если выбираем редактирование кода_html, подключим к CodeMirror тему blackboard
                    if (e_type == '2') {
                        editor.setOption("theme", "base16-dark");
                        editor.setOption("mode", "text/html");
                     };
                     //если выбираем редактирование кода_markdown, подключим к CodeMirror тему default
                     if (e_type == '1') {         
                        editor.setOption("theme", "base16-light");
                        editor.setOption("mode", "text/html");
                     };
                    
                });
			
                //если block_image не пустое то выводим картинку + ссылку на удаление картинки
                if(block_image){
                $('#img_holst').append('<div id="img_holst_inner"><a href="#" data-block_id="' + block_id + '" data-block_image="' + block_image + '" id="delete_img"><i class="fa fa-times-circle" aria-hidden="true"></i></a><img src="/images/crop/' + block_image + '"/></div>'); 
                }
                
              });
                         
              //МОДАЛЬНОЕ ОКНО РЕДАКТИРОВАНИЯ СТРОКИ
              $('body').on('click','.row_edit',function(){
                //открыть модальное окно id="add_edit_row" с формой редакт
                $("#add_edit_row").modal('show');
                //кнопка вызова модального окна имеет атрибут data с данными - эти данные заносим в переменные
                var r_title = $(this).data('r_title');
                var r_position = $(this).data('r_position');
                var r_id = $(this).data('r_id');

                //заносим данные в поля формы - для редактирования блока
                $("form#my_row").attr("action","/index.php/admin/edit_row");
                //скрываем поле - не участвует в редактировании, используется при создании строки
                $("#b_generation").css({"display":"none"});
                $("input#rows_title").val(r_title);
                $("input#rows_position").val(r_position);
                $("input#r_id").val(r_id);
              });
              
              //Удаление фото по клику - модальное окно добавления/редактирования блока
              $('body').on('click','#delete_img',function(event){
                event.preventDefault();
                //удаляем фото в модальном окне
                $('#img_holst').empty();
                //удаляем имя файла из скрытого поля формы
                $('#b_image').val('');
                $('#img_holst').prepend('<div class="alert alert-warning" role="alert">Изображение удалено, изменения вступят в силу после нажатия кнопки "Отправить" внизу окна.</div>');
              });
              
              //очищаем модальное окно с формой строки после его закрытия
              $('#add_edit_row').on('hidden.bs.modal', function () {
                 //очищаем форму
                 $('form#my_row input').val('');
              });
              
              //Сортировка перетаскивание строк
              Sortable.create(simpleList, {animation: 200,
                store: {
                // Получение сортировки (вызывается при инициализации)
                get: function (sortable) {
                var order = sortable.toArray();
                return order;
                },
                // Сохранение сортировки (вызывается каждый раз при её изменении)
                set: function (sortable) {
                var order = sortable.toArray();
                console.log(order);
                    jQuery.ajax({
                        type: "POST",
                        url: "/index.php/admin/json_sort",
                        data: {id_row: order},
                        success: function(rows){
                        console.log(rows);
                        }
                    });
                 }
                 }
              });
              
              //МОДАЛЬНОЕ ОКНО добавления пунктов навигации
              $('body').on('click','#nav_add',function(){
                //открыть модальное окно id="editblock" с формой редакт блока
                $("#add_nav_items").modal('show');
              });
                
              //МОДАЛЬНОЕ ОКНО РЕДАКТИРОВАНИЯ пунктов навигации
              $('body').on('click','#nav_del_edit',function(){
                //открыть модальное окно id="editblock" с формой редакт блока
                $("#edit_nav_items").modal('show');
              });
              
              //МОДАЛЬНОЕ ОКНО РЕДАКТИРОВАНИЯ определённого пункта навигации
              $('body').on('click','#nav_one_edit',function(){
                //открыть модальное окно id="editblock" с формой редакт блока
                $("#edit_nav_one_item").modal('show');
                //кнопка вызова модального окна имеет атрибут data с данными - эти данные заносим в переменные
                var nav_one_id = $(this).data('nav_one_id');
                var nav_one_name = $(this).data('nav_one_name');
                var nav_one_veight = $(this).data('nav_one_veight');
                var nav_one_visible = $(this).data('nav_one_visible');
                var nav_one_url = $(this).data('nav_one_url');
                //заносим данные в поля формы - для редактирования пункта навигации
                $("input#li_id").val(nav_one_id);
                $("input#li_name").val(nav_one_name);
                $("input#li_url").val(nav_one_url);
                $("select#li_veight").val(nav_one_veight);
                $("select#li_visible").val(nav_one_visible);
              });
            
            //Раскрыть или свернуть все блоки
            $('#all_collapse').on('click', function () {
                $('.collapse').collapse('toggle');
            });
            
            //навигационная форма - добавляем поля для создания пунктов меню
            (function(){
            var a = 0;
            $("body").on('click', '#addd', function() {
                            a = a + 1;
                            //помещаем атрибуты полей формы в переменную
                            var b = 'items[' + a + '][url]';
                            var c = 'items[' + a + '][li_name]';
                            var d = 'items[' + a + '][visible]';
                            var e = 'items[' + a + '][veight]';
                            //клонируем div с полями
                            $("#nav_field").clone().attr('id', 'nav_field_' + a).appendTo("#clon_place");
                            //переписываем атрибут склонированных полей
                            $('#nav_field_' + a + ' select.url').attr("name", b);
                            $('#nav_field_' + a + ' input.li_name').attr("name", c);
                            $('#nav_field_' + a + ' input.visible').attr("name", d);
                            $('#nav_field_' + a + ' select.veight').attr("name", e);
            });
            })();//end of navigation form


}); 