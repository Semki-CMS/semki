<div class="container">
	    <h1>Создание формы обратной связи</h1>
		<div class="panel panel-default">
			<div class="panel-body">
				<?php
					$attributes = array('id' => 'frm_fields');
					echo form_open('admin/field_form_db', $attributes);
					//генерируем уникальный id
					$id_token = uniqid();
				?>
					<div class="row">
						<div class="col-md-3">
								<label>Название формы</label> <sup> *</sup>
								<input type="text" name="f_form_name" class="form-control input-sm"  required/>			
						</div>
						<div class="col-md-3">
								<label>Надпись на кнопке отправки</label> <sup> *</sup>
								<input type="text" name="button" class="form-control input-sm" required/>
						</div>
						<div class="col-md-2" style="opacity: 0.33;">
								<label>Id формы</label>
								<input type="text" name="f_form_id" class="form-control input-sm" value="<?php echo $id_token ?>" />
						</div>
					</div>
					<hr>
					
					<div id="my_field" class="row">
						<div class="col-md-2">
							<label>Название поля</label> <sup> *</sup>
							<input type="text" name="items[0][field_label]" class="form-control input-sm field_label"  required/>			
						</div>

						<div class="col-md-2">
							<label>Тип поля</label>
							<select name="items[0][field_type]" class="form-control input-sm field_type">
								<option value="text" selected="selected">Текстовое поле</option>
								<option value="number">Числовое поле</option>
								<option value="email">Email</option>
								<option value="textarea">Текстовая область</option>
							</select>
						</div>
						<div class="col-md-2">
							<label>Placeholder</label>
							<input type="text" name="items[0][placeholder]" class="form-control input-sm placeholder"/>	
						</div>
						<div class="col-md-2">
							<label>Обязательное поле</label>
							<select name="items[0][required]" class="form-control input-sm required">
								<option value="1">да</option>
								<option value="0">нет</option>
							</select>
						</div>
						<div class="col-md-2">	
							<label>Вес поля</label>
							<select name="items[0][veight]" class="form-control input-sm veight">
								<option value="0" selected="selected">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
							</select>
						</div>
						<div class="col-md-2" style="opacity: 0.33;">
								<label>Id формы</label>
								<input type="text" name="items[0][form_id]" class="form-control input-sm form_id" value="<?php echo $id_token ?>" />
						</div>
						<div class="clear_6"></div>
					</div>
					<div id="clon_my_place"></div>
				</form>
				<button class="btn btn-default btn-sm" id="ad_field">Ещё</button>
			</div>
			<div class="panel-footer">
				<input  form="frm_fields" class="btn btn-default btn-sm" type="submit" value="Добавить форму" />
			</div>
		</div>
	</form>

</div> <!-- end of conteiner -->

<script>
$(document).ready(function() {
			//форма - добавляем поля для создания формы обратной связи (см. файт admin_tpl/creator_forms)
            (function(){
            var a = 0;
            $("body").on('click', '#ad_field', function() {
                            a = a + 1;
                            //помещаем атрибуты полей формы в переменную
                            var b = 'items[' + a + '][field_label]';
                            var c = 'items[' + a + '][field_type]';
                            var d = 'items[' + a + '][veight]';
							var e = 'items[' + a + '][form_id]';
                            //клонируем div с полями
                            $("#my_field").clone().attr('id', 'my_field_' + a).appendTo("#clon_my_place");
							//удаляем label.error если он был склонирован
							$('#my_field_' + a + ' label.error').remove();
                            //переписываем атрибут склонированных полей
                            $('#my_field_' + a + ' input.field_label').attr("name", b);
                            $('#my_field_' + a + ' select.field_type').attr("name", c);
							$('#my_field_' + a + ' select.veight').attr("name", d);
							$('#my_field_' + a + ' input.form_id').attr("name", e);

            });
            })();//end 

}); 
</script>