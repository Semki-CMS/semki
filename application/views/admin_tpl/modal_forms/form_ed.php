<!-- Modal редактировать форму -->
<div class="modal fade" id="win_ed_form">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Редактировать форму</h4>
			</div>
		  
			<div class="modal-body">
				<ul class="nav nav-tabs">
				  <li class="active"><a data-toggle="tab" href="#panel1">Редактировать форму</a></li>
				  <li><a data-toggle="tab" href="#panel2">Редактировать поля</a></li>
                  <li><a data-toggle="tab" href="#panel3">Добавить поле</a></li>
				</ul>
				<div class="tab-content">
					<div id="panel1" class="tab-pane fade in active">
						<?php
							echo form_open('admin/edit_form_db');
						?>
							<div class="row form-group">
								<div class="col-md-3">
										<label>Название формы</label>
										<input type="text" id="f_form_name" name="f_form_name" class="form-control input-sm" value=""  required />			
								</div>
								<div class="col-md-3">
										<label>Надпись на кнопке отправки</label>
										<input type="text" id="f_form_button" name="button" class="form-control input-sm" value="" required />
								</div>
								<div class="col-md-3" style="opacity: 0.33;">
										<label>Id формы</label>
										<input type="text" id="f_form_id" name="f_form_id" class="form-control input-sm" value="" required />
								</div>
							</div>
							<input  class="btn btn-primary btn-sm" type="submit" value="Отредактировать форму" />
						</form>
					</div>
                    <div id="panel2" class="tab-pane fade">
                        <div>
                        <!-- в блок #json_cicle выводим поля с помощью js-цикла - смотри файл /admin_tpl/forms_view -->
						<table id="json_cicle" class="table table-bordered">
							<tr>
								<th>Ярлык</th>
								<th>Id</th>
								<th>Тип</th>
								<th>Placeholder</th>
								<th>Обязательность</th>
								<th>Вес</th>
								<th>Операции</th>
							</tr>
						</table>
                        <div id="field_null"></div>
                        </div>
					</div><!-- end of panel2 -->  
					<div id="panel3" class="tab-pane fade">	
						<?php
							echo form_open('admin/edit_fields_db');
						?>
							<div class="row form-group">
								<div class="col-md-2">
									<label>Ярлык поля</label>
									<input type="text" name="field_label" class="form-control input-sm field_label"  required/>			
								</div>

								<div class="col-md-2">
									<label>Тип поля</label>
									<select name="field_type" class="form-control input-sm field_type">
										<option value="text" selected="selected">Текстовое поле</option>
										<option value="number">Числовое поле</option>
										<option value="email">Email</option>
										<option value="textarea">Текстовая область</option>
									</select>
								</div>
								<div class="col-md-2">
									<label>Placeholder</label>
									<input type="text" name="placeholder" class="form-control input-sm placeholder"/>	
								</div>
								<div class="col-md-2">
									<label>Обязательность</label>
									<select name="required" class="form-control input-sm required">
										<option value="1">да</option>
										<option value="0">нет</option>
									</select>
								</div>
								<div class="col-md-2">	
									<label>Вес поля</label>
									<select name="veight" class="form-control input-sm veight">
										<option value="0" selected="selected">0</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
									</select>
								</div>
								<div class="col-md-2" style="opacity: 0.33;">
										<label>Id формы</label>
										<input type="text" name="form_id_sec" id="form_id" class="form-control input-sm form_id" value="" />
								</div>
							</div>
							<div class="row">
								<div class="col-md-2">
									<input class="btn btn-primary btn-sm" type="submit" value="Добавить поле" />
								</div>
							</div>
						</form>
					</div><!--end of panel-->                  
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal редактировать поле формы -->
<div class="modal fade" id="win_ed_field">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" >Редактировать поле</h4>
			</div>
            <div class="modal-body">
            	<!-- сюда приходят сообщения "message" из сессии (одноразовые сообщения формируем с помощью set_flashdata в контроллере Admin.php перед редиректом) -->
            	<?php
            	if ($this->session->flashdata('message')){
            	echo "<div class='".$this->session->flashdata('message_type')."' id='flashdata'>  <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>".$this->session->flashdata('message')."</div>";
            	} ?>
				<?php
					$attributes = array('id' => 'file_ed');
					echo form_open('admin/update_edit_form_field', $attributes);
				?>
				      <div class="form-group">
        				   <label>Ярлык поля</label><br/>
        				   <input type="text" name="field_label" id="field_label" class="form-control input-sm" value="" />
                      </div>
                      <div class="form-group">
                            <label>Placeholder</label><br/>
					        <input type="text" id="field_placeholder" name="placeholder" class="form-control input-sm" value=""/>
                      </div>
					  <div class="form-group">
						    <label>Тип поля</label><br/>
                            <select name="field_type" id="field_type" class="form-control input-sm">
    							<option value="text">Текстовое поле</option>
                                <option value="number">Числовое поле</option>
                                <option value="email">Email</option>
                                <option value="tel">Телефон</option>
							</select>
                        </div>
					    <div class="form-group">
							<label>Обязательное поле</label><br/>
                            <select name="requiredt" id="field_required" class="form-control input-sm">
    							<option value="1">да</option>
                                <option value="0">нет</option>
                            </select>
					    </div>
						<div class="form-group">	
							<label>Вес поля</label><br/>
                            <select name="veight" id="field_veight" class="form-control input-sm">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            </select>                                        
						</div>
                    
					<div class="row form-group">
						<div class="col-md-6" style="opacity: 0.33;">
								<label>Id формы</label><br/>
								<input type="text" id="form_id" name="form_id" class="form-control input-sm form_id" value=""/>
						</div>
						<div class="col-md-6" style="opacity: 0.33;">
								<label>Id поля</label><br/>
								<input type="text" id="field_id" name="field_id" class="form-control input-sm form_id" value=""/>
						</div>
					</div>
				</form>
            </div>
            <div class="modal-footer">
           	    <input form="file_ed" class="btn btn-primary btn-sm" type="submit" value="Сохранить" />
            </div>
        </div>
    </div>
</div>