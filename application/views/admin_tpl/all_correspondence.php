<div class="container">
    <div style="margin:10px 0 0;" class="my_breadcrumb"><a href="/">Главная</a> » Письма</div>
    <h1>Письма</h1>
	<div class="panel panel-default">
		<div class="panel-body">
            <?php echo $this->pagination->create_links();?>
			<table class="table table-bordered">
			   <tr>
				<th>#</th>
				<th>Id формы</th>
				<th>Ip пользователя</th>
				<th>Дата</th>
				<th>Операции</th>
			   </tr>
			<?php foreach($corresp as $value): ?>
				<tr>
					<td>
						<?php echo $value['corr_id']; ?>
					</td>
					<td>
						<?php echo $value['form_id']; ?>
					</td>
					<td>
						<?php echo $value['user_ip']; ?>
					</td>
					<td>
						<?php echo $value['post_time']; ?>
					</td>
					<td>
						<a href="/admin/one_correspondence/<?php echo $value['corr_id']; ?>">просмотр</a>
					</td>
				</tr>
			<?php endforeach; ?>

			</table>
		</div>
	</div>
</div>