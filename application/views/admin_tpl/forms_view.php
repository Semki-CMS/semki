<div id="nav_top_edit">
    <div class="container">
		<div style="margin:10px 0 0;" class="my_breadcrumb"><a href="/">Главная</a> » <a href="/admin">Шаблон</a> » Список форм</div>
		<h1>Списик форм обратной связи</h1>
    </div>
</div>
<div class="container">

	<!-- сюда приходят сообщения "message" из сессии (одноразовые сообщения формируем с помощью set_flashdata в контроллере Admin.php перед редиректом) -->
	<?php
	if ($this->session->flashdata('message')){
	echo "<div class='".$this->session->flashdata('message_type')."' id='flashdata'>  <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>".$this->session->flashdata('message')."</div>";
	} ?>
        
	<div class="row">
	    <div class="col-md-6">
			<?php foreach($all_forms as $key => $value): ?>
				<?php
					$attributes = array('class' => 'push_form');
					echo form_open('admin/send_form', $attributes);
				?>
					<div class="panel panel-default">
						<!-- Default panel contents -->
						<div class="panel-heading">
							<div style="float:left;"><?php echo $value[0]['f_form_name'] ?> <span style="color: #ccc;margin-left: 5px;">(id: <?php echo $key ?>)</span></div>
							<div style="float:right;"><a href="#" class="font-avesome ed_form" title="Редактировать форму" data-form_id="<?php echo $key ?>" data-form_name="<?php echo $value[0]['f_form_name'] ?>" data-form_button="<?php echo $value[0]['button'] ?>" ><i class="fa fa-pencil"></i></a></div>
							<div style="clear:both;"></div>
						</div>
						<div class="panel-body">
							<!-- скрытое поле - передаём id формы с которой отправляем данные -->
							<input type="hidden" name="form_id" value="<?php echo $key; ?>" />
							<?php if(!empty($value[0]['field_id'])):?> <!-- если есть поля - выводим их -->
							<?php foreach($value as $form): ?>
								<div class="form-group">
									<label><?php echo $form['field_label'] ?></label><?php if (!empty($form['required'])): ?><sup> *</sup><?php endif; ?>
									<input type="hidden" name="label_<?php echo $form['field_id'] ?>" value="<?php echo $form['field_label']; ?>" />
									<?php if($form['field_type'] == 'textarea'):?>
									<div class="row">
										<div class="col-md-9">
											<textarea class="form-control textarea-sm" name="<?php echo $form['field_id'] ?>" placeholder="<?php echo $form['placeholder'] ?>" <?php if ($form['required'] == 1){ echo 'required';} ?>></textarea>
										</div>
										<div class="col-md-3" style="padding-top: 4px;">
											<a href="/index.php/admin/edit_form_field/<?php echo $form['field_id'] ?>" class="font-avesome" title="Редактировать поле"><i class="fa fa-pencil"></i></a>
											<a href="/index.php/admin/delete_form_field/<?php echo $form['field_id'] ?>" class="font-avesome" title="Удалить поле" onclick="return window.confirm('Вы уверены что хотите удалить поле?');"><i class="fa fa-times"></i></a>
										</div>
									</div>
									<?php else:?>
									<div class="row">
										<div class="col-md-9">
											<input class="form-control input-sm" name="<?php echo $form['field_id'] ?>" type="<?php echo $form['field_type'] ?>" placeholder="<?php echo $form['placeholder'] ?>" <?php if ($form['required'] == 1){ echo 'required';} ?>/>
										</div>
										<div class="col-md-3" style="padding-top: 4px;">
											<a href="#" data-field_id="<?php echo $form['field_id'] ?>" data-form_id="<?php echo $form['form_id'] ?>" data-field_label="<?php echo $form['field_label'] ?>" data-field_type="<?php echo $form['field_type'] ?>" data-field_placeholder="<?php echo $form['placeholder'] ?>" data-field_required="<?php echo $form['required'] ?>" data-field_veight="<?php echo $form['veight'] ?>" class="font-avesome ed_field" title="Редактировать поле"><i class="fa fa-pencil"></i></a>
											<a href="/index.php/admin/delete_form_field/<?php echo $form['field_id'] ?>" class="font-avesome" title="Удалить поле" onclick="return window.confirm('Вы уверены что хотите удалить поле?');"><i class="fa fa-times"></i></a>
										</div>
									</div>
									<?php endif?>
								</div>
							<?php endforeach; ?>
							
							<?php else: ?>
								<p>Пока полей нет</p>
							<?php endif; ?>
						</div>

						<div class="panel-footer">
							<input  class="btn btn-default btn-sm" type="submit" value="<?php echo $value[0]['button'] ?>" />
						</div>
					</div>
				</form>
			<?php endforeach; ?>
        </div>
	</div>
</div> <!-- end conteiner -->
<script>
$(document).ready(function() {
      //МОДАЛЬНОЕ ОКНО РЕДАКТИРОВАНИЯ формы
      $('body').on('click','.ed_form',function(){
        //открыть модальное окно
        $("#win_ed_form").modal('show');

        //кнопка вызова модального окна имеет атрибут data с данными - эти данные заносим в переменные
        var form_name = $(this).data('form_name');
        var form_button = $(this).data('form_button');
        var form_id = $(this).data('form_id');

        //заносим данные в поля формы - первая вкладка в модальном окне - редактирование формы
        $("input#f_form_name").val(form_name);
        $("input#f_form_button").val(form_button);
        $("input#f_form_id").val(form_id);
        //заносим данные в поля формы - вторая вкладка в модальном окне - редактирование полей
        $("input#form_id").val(form_id);
        
        //Переводим массив с данными форм в JSON. Так как выбор формы происходит по клику, id-формы как php-переменную передать в цикл не удастся, поэтому цикл делаем на JS + JSON
        var forms_json = '<?php echo json_encode($all_forms); ?>';
        //передаем строку методу JSON.parse(), который создает объект
        var forms_obj = jQuery.parseJSON(forms_json);
        
        //очищаем "#json_cicle td" перед записью данных цикла
        $('#json_cicle td').remove();
        //заносим в переменную массив обьектов(данных полей формы) связанных с формой, которую мы редактируем
        var obj = forms_obj[form_id];
        
        //в цикле выводим наши поля, относящиеся к форме, по которой мы кликнули для редактирования
        //условие - если поля есть
        if (obj[0].field_id !== null) 
        {
            //выведем поля в цикле предварительно очистив #field_null
            obj.forEach(function(item, i) {
              $('#field_null').html('');
              $('#json_cicle').append('<tr><td>' + item.field_label + '</td><td>' + item.field_id + '</td><td>' + item.field_type + '</td><td>' + item.placeholder + '</td><td>' + item.required + '</td><td>' + item.veight + '</td><td><a href="#" data-form_id="' + item.form_id + '" data-field_id="' + item.field_id + '" data-field_label="' + item.field_label + '" data-field_type="' + item.field_type + '" data-field_placeholder="' + item.placeholder + '" data-field_veight="' + item.veight + '" data-field_required="' + item.required + '" class="font-avesome ed_field" title="Редактировать поле"><i class="fa fa-pencil"></i></a><a href="/index.php/admin/delete_form_field/' + item.field_id + '" class="font-avesome" title="Удалить поле" onclick="return window.confirm(\'Вы уверены что хотите удалить поле?\');"><i class="fa fa-times"></i></a></td></tr>');
            });
        } else {
           $('#field_null').append('Полей нет');
                }
        //console.log( obj );
        //alert(obj[0].f_form_id);
      });
      
      //МОДАЛЬНОЕ ОКНО РЕДАКТИРОВАНИЯ поля формы
      $('body').on('click','.ed_field',function(){
        //открыть модальное окно
        $("#win_ed_field").modal('show');
        //кнопка вызова модального окна имеет атрибут data с данными - эти данные заносим в переменные
        var form_id = $(this).data('form_id');
        var field_id = $(this).data('field_id');
        var field_required = $(this).data('field_required');
        var field_placeholder = $(this).data('field_placeholder');
        var field_veight = $(this).data('field_veight');
        var field_label = $(this).data('field_label');
        var field_type = $(this).data('field_type');
        //заносим данные в поля формы - первая вкладка в модальном окне - редактирование формы
        $("input#field_id").val(field_id);
        $("input#field_label").val(field_label);
        $("input#field_placeholder").val(field_placeholder);
        $("select#field_veight").val(field_veight);
        $("select#field_required").val(field_required);
        $("select#field_type").val(field_type);
        $("input#form_id").val(form_id);
      });
});
</script>