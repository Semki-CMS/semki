<div class="container">
    <div style="margin:10px 0 0;" class="my_breadcrumb"><a href="/">Главная</a> » Письмо <?php echo $corresp[0]['corr_id'] ?></div>
	<div id="corresp" class="row">
	    <div class="col-md-6">
		    <h1>Письмо <?php echo $corresp[0]['corr_id'] ?></h1>
            <div class="panel panel-default">
                <div class="panel-body">
        		    <p><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $corresp[0]['post_time'] ?></p>
        			
        			<div class="well well well-sm">
        				<ul>
        					<li>id письма: <?php echo $corresp[0]['corr_id'] ?></li>
        					<li>id формы: <?php echo $corresp[0]['form_id'] ?></li>
        					<li>ip автора: <?php echo $corresp[0]['user_ip'] ?></li>
        				</ul>
        			</div>
        			
        			<?php foreach($corresp as $value): ?>
        			    <!-- если vaue поля не пустое - выводим его -->
        				<?php if(!empty($value['field_value'])):?>
        				    <!-- "$string = substr" - обрезаем 5 символов, получим класс со значением label  -->
        					<div class="<?php $string = substr($value['field_id'], 0, 5); echo $string ?>"><?php echo $value['field_value'] ?></div>
        				<?php else: ?>
        					<div class="err_txt">Поле не заполнено</div>
        				<?php endif ?>	
        			<?php endforeach; ?>
    			</div>
            </div>
		</div>
	</div>
</div>