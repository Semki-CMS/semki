<div  class="row" id="my_head">
    <div class="container">
         <!-- Static navbar -->
         <nav id="site_nav" class="navbar navbar-default">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Semki</a>
              </div>
              <div class="collapse navbar-collapse" id="navbar">
                    <ul id="nav_scroll" class="nav navbar-nav navbar-right">
                        <?php foreach($nav_value as $key => $value): ?>
                            <li><a href="<?php echo $value['url']?>"><?php echo $value['li_name']?></a></li>
                        <?php endforeach; ?>
                    </ul>
              </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
         </nav>
    </div>
</div>
     
<div class="container">

<!-- сюда приходят сообщения "message" из сессии (одноразовые сообщения формируем с помощью set_flashdata в контроллере Admin.php перед редиректом) -->
<?php
if ($this->session->flashdata('message')){
echo "<div class='".$this->session->flashdata('message_type')."' id='flashdata'>  <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>".$this->session->flashdata('message')."</div>";
} ?>

<?php foreach($my_rows as $key => $value): ?>
        <!-- id="anchor_$key" used in navigation for scrolling to the appropriate anchor -->
        <div id="anchor_<?php echo $key ?>" data-id="<?php echo $key?>" class="row sortable">
            
            <?php foreach($value as $_block): ?>
                    <div class="col-md-<?php echo $_block['col_width']?>">
                        <div  id="m_block_<?php echo $_block['b_id']?>" class="m_block">
                        
                            <?php if (!empty($_block['b_title'])): ?> 
                                <div class="block_title"><?php echo $_block['b_title']?></div>
                            <?php endif; ?>
                            
                            <div>
                                <?php if (!empty($_block['image'])): ?>  
                                <div class="block_image"><img title="<?php echo $_block['title']?>" alt="<?php echo $_block['title']?>" src="<?php echo base_url()?>images/crop/<?php echo $_block['image']?>"/></div>
                                <?php endif; ?>
                                <?php if (!empty($_block['text'])): ?> 
                                <div class="block_text"><?php echo $_block['text']?></div>
                                <?php endif; ?>
								<!-- form attach -->
								<!-- если блок имеет значение аттача = id-формы (которая и будет приаттачена) -->
								<?php if (!empty($_block['attach_form_id'])): ?>
									<div>
										<?php
										echo form_open('admin/send_form');
										?>
										<!-- в скрытом поле передадим id формы (которую приаттачим) -->
										<input type="hidden" name="form_id" value="<?php echo $_block['attach_form_id']; ?>" />
											<?php foreach($all_forms[$_block['attach_form_id']] as $value): ?>
											    <div class="form-group">
												<label><?php echo $value['field_label'] ?></label><?php if (!empty($value['required'])): ?><sup> *</sup><?php endif; ?>
												<input type="hidden" name="label_<?php echo $value['field_id'] ?>" value="<?php echo $value['field_label']; ?>" />
												<?php if($value['field_type'] == 'textarea'):?>
												<textarea class="form-control textarea-sm" name="<?php echo $value['field_id'] ?>" placeholder="<?php echo $value['placeholder'] ?>" <?php if ($value['required'] == 1){ echo 'required';} ?>></textarea>
												<?php else:?>
												<input class="form-control input-sm" name="<?php echo $value['field_id'] ?>" type="<?php echo $value['field_type'] ?>" placeholder="<?php echo $value['placeholder'] ?>" <?php if ($value['required'] == 1){ echo 'required';} ?>/>
												<?php endif?>
											    </div>
											<?php endforeach; ?>
											<div>
												<input class="btn btn-default btn-sm" type="submit" value="<?php echo $value['button'] ?>" />
											</div>
										</form>
									</div>
								<?php endif; ?> <!-- end of attach -->
                            </div>
                            
                        </div>
                    </div>
             <?php endforeach; ?>
        </div>
<?php endforeach; ?>
    
</div>