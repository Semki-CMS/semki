<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
        
        public function get_rows()
        {
            $this->db->select('*');
            $this->db->from('blocs');
            //'right outer' выводит строки если даже они пустые
            $this->db->join('rows', 'rows.r_id = blocs.id_row', 'right outer');
            $this->db->order_by('r_position', "asc");
            $this->db->order_by("b_position", "asc");
            $query = $this->db->get();
            
            return $query->result_array();
        }
        
        public function get_navigation()
        {
            $this->db->select('*');
            $this->db->from('navigation');
            $this->db->where('visible', '1');
            $this->db->order_by("veight", "asc");
            $query = $this->db->get();
            
            return $query->result_array();
        }
		
		//выводим все формы с полями для страницы admin/forms
		public function get_forms()
        {
            $this->db->select('*');
            $this->db->from('forms_id');
			//'left outer' выводит форму если даже она пока не имеет полей
			$this->db->join('form_fields', 'form_fields.form_id = forms_id.f_form_id', 'left outer');
			$this->db->order_by('veight', "asc");
            $query = $this->db->get();
            
            return $query->result_array();
        }
		
		//выводим только id форм, используется для выбора формы из выпадающего списка (которую необходимо приаттачить) при создании блока.
		public function get_only_forms()
        {
            $this->db->select('*');
            $this->db->from('forms_id');
            $query = $this->db->get();
            
            return $query->result_array();
        }
		
        //достаём данные о полях формы обратной связи
		public function get_form_field($id)
        {
			//данные таблицы поля
            $this->db->select('*');
            $this->db->from('form_fields');
			$this->db->where('field_id' , $id);
            $query = $this->db->get();
            return $query->result_array();
        }
		
		//достаём данные о всей корреспонденции
		public function get_all_corresp($num, $offset)
        {
            $this->db->order_by("post_time", "desc"); 
            $query = $this->db->get('correspondence', $num, $offset);

            return $query->result_array();
        }
		
		//достаём данные одного письма
		public function get_one_corresp($id)
        {
			$this->db->select('*');
            $this->db->from('correspondence');
			$this->db->where('corr_id' , $id);
            $this->db->join('correspondence_fields', 'correspondence_fields.reff_id = correspondence.corr_id');
            $this->db->order_by('id', "asc");
            $query = $this->db->get();
			
			return $query->result_array();
        }
        
        //достаём данные настроек сайта
        public function get_settings()
        {
            $this->db->select('*');
            $this->db->from('settings');
            $query = $this->db->get();
            
            return $query->row_array();
        }
        
}