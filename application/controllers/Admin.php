<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct()
        {
            parent::__construct();
            $this->load->model('Admin_model');
            $this->load->helper('url');
            $this->load->helper('form');
            
            //для работы с textarea - <br><p> итд
            $this->load->library('typography');
            $this->load->library('ion_auth');
        }

	public function index()
    	{
            // выводим массив навигации
            $data['nav_value'] = $this->Admin_model->get_navigation();
			
			// выводим массив форм, используем в форме создания блока, для того чтобы приаттачить форму к блоку
            $data['attach_form_id'] = $this->Admin_model->get_only_forms();
			
			// выводим массив форм
            $all_forms = $this->Admin_model->get_forms();
			//формируем массив (для дальнейшего вывода форм)
            foreach ($all_forms as $l) {
            $data['all_forms'][$l['f_form_id']][] = $l;
            }
                        
    	    //формируем массив (для дальнейшего вывода блоков в строках)
            $my_rows = $this->Admin_model->get_rows();
            foreach ($my_rows as $l) {
            $data['my_rows'][$l['r_id']][] = $l;
            }
            
            //теперь из обработанного массива достаём ключи (они же айди строк)
            //эту последовательность ключей помещаем в переменную $data['option_row']
            //и используем для выбора строки при создании блока в админке
            $a = array_keys($data['my_rows']);
            $data['option_row'] = $a;
            
            //если админ - загружаем страничку
            if ($this->ion_auth->is_admin())
            { 
                $this->load->view('admin_tpl/header');
                $this->load->view('admin_tpl/admin_nav');
        		$this->load->view('admin_tpl/admin_index', $data);
                $this->load->view('admin_tpl/modal_forms/tpl_ed');
            } 
            else  
            { 
                 redirect('/auth', 'refresh');
            }
            
            //echo "<pre>";
            //print_r($data['nav_value']);
            //print_r($this->session->all_userdata());
            //echo "</pre>";
    	}
        
        // сортируем строки перетаскиванием - данные приходят аяксом
        public function json_sort()
    	{ 
            // формируем массив из значений айди строки
    	    $data = array(
                    '_rows' => $this->input->post('id_row')
                );
            // перебираем и пишем очерёдность после сортировки 
            // согласно position для для каждого id_row
            $pos_new = 1;
            foreach($data['_rows'] as $item){
                $this->db->where('r_id', $item);
                $this->db->update('rows', array('r_position' => $pos_new));
            	$pos_new++;
            }
            
            //возвращаем для теста
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
    	}
        
        // обработчик - добавление строки с блоком
        public function add_row()
    	{   //массив для записи строки
            $data_row = array(
                'title' => $this->input->post('rows_title'),
                'r_position' => $this->input->post('rows_position')
            );
            
            //количество создаваемых блоков - используем в цикле добавляющем блоки
            $count_blocks = $this->input->post('data_bcount');
            //по количеству блоков вычисляем ширину каждого
            $full_width = 12;
            $block_width = $full_width / $count_blocks;
            
            $this->db->trans_start(); //старт транзакции
            
            //добавим регион-строку
            $this->db->insert('rows', $data_row);
            
            //массив для записи блоков
            $data_block = array(
                // айди вновь созданной строки - пишем в поле 'id_row' блока для связки блок-строка
                'id_row' => $this->db->insert_id(),
                'col_width' => $block_width,
            );
            
            //добавим блоки в цикле
            for ($x=0; $x<$count_blocks; $x++)
            {
                $this->db->insert('blocs', $data_block);
            }
            
            $this->db->trans_complete(); //финиш транзакции
			
			//заносим сообщение об успешном создании строки - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Строка <strong>"' . $data_row['title'] . '"</strong> с блоками успешно создана');
            $this->session->set_flashdata($message_data);
            
            redirect('/admin', 'refresh');
    	}
        
        // обработчик - добавление пунктов меню
        public function add_nav_li()
    	{
            $data = array(
                'items' => $this->input->post('items'),
            );
            
            foreach($data['items'] as $value){
                $this->db->insert('navigation', $value);
            }
            //заносим сообщение об успешном добавлении пункта меню - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Пункт(ы) меню успешно добавлен(ы)');
            $this->session->set_flashdata($message_data);
            
            redirect('/admin', 'refresh');
    	}
        
        //обработчик - редактируем пункт меню
        public function edit_nav_li()
    	{
           $data = array(
                'li_name' => $this->input->post('li_name'),
                'visible' => $this->input->post('visible'),
                'id' => $this->input->post('li_id'),
                'veight' => $this->input->post('veight')
            );

            $this->db->where('id', $data['id']);
            $this->db->update('navigation', $data);
            
            //заносим сообщение об успешном редактировании пункта меню - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Пункт <strong>"' . $data['li_name'] . '"</strong> отредактирован (id: ' . $data['id'] . ')');
            $this->session->set_flashdata($message_data);
            
            redirect('/admin', 'refresh');
            //print_r ($data);

    	}
        
        // обработчик - удаляем пункт меню
        public function delete_nav_li($id)
    	{
            $this->db->delete('navigation', array('id' => $id));
            
            //заносим сообщение об успешном удалении пункта меню - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Пункт меню удалён (id: ' . $id . ')');
            $this->session->set_flashdata($message_data);
            
            redirect('/admin', 'refresh');
    	}
        
        // обработчик - добавление одного блока
        public function add_block()
    	{
            //массив для записи блока
            $data = array(
                'b_title' => $this->input->post('b_title'),
                'text' => $this->input->post('text'),
                'col_width' => $this->input->post('b_width'),
                'b_position' => $this->input->post('b_position'),
                'image' => $this->input->post('b_image'),
				//приаттачим к блоку форму
				'attach_form_id' => $this->input->post('attach_form_id'),
                'id_row' => $this->input->post('id_row')
            );
            
            //узнаем какой тип обработки textarea выбран
            $editor_type = $this->input->post('editor_type');
            if($editor_type == 1){
            //если выбран редактор текста, преобразуем текст используя библиотеку 'typography'
            $data['text'] = $this->typography->auto_typography($data['text']);
            }
            
            $this->db->insert('blocs', $data);
			
			//заносим сообщение об успешном создании блока - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Блок <strong>' . $data['b_title'] . '</strong> успешно создан');
            $this->session->set_flashdata($message_data);
			
            redirect('/admin', 'refresh');
    	}
        
                
        // обработчик - редактирование блока
        function edit_block()
        {
            $data = array(
                'b_title' => $this->input->post('b_title'),
                'text' => $this->input->post('text'),
                'col_width' => $this->input->post('b_width'),
                'id_row' => $this->input->post('id_row'),
                'b_position' => $this->input->post('b_position'),
                'image' => $this->input->post('b_image'),
				'attach_form_id' => $this->input->post('attach_form_id'),
                'b_id' => $this->input->post('b_id')
            );
            
            //узнаем какой тип обработки textarea выбран
            $editor_type = $this->input->post('editor_type');
            if($editor_type == 1){
            //если выбран редактор текста, преобразуем текст используя библиотеку 'typography'
            $data['text'] = $this->typography->auto_typography($data['text']);
            }
            
            $this->db->where('b_id', $data['b_id']);
            $this->db->update('blocs', $data);
			
			//заносим сообщение об успешном редактировании блока - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Блок <strong>' . $data['b_title'] . '</strong> успешно отредактирован');
            $this->session->set_flashdata($message_data);
            
            redirect('/admin', 'refresh');
        }
        
        // обработчик - редактирование строки
        function edit_row()
        {
            $data = array(
                'title' => $this->input->post('rows_title'),
                'r_id' => $this->input->post('r_id'),
                'r_position' => $this->input->post('rows_position')
            );
            
            $this->db->where('r_id', $data['r_id']);
            $this->db->update('rows', $data);
			
			//заносим сообщение об успешном редактировании строки - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Строка <strong>"' . $data['title'] . '"</strong> успешно отредактирована');
            $this->session->set_flashdata($message_data);
            
            redirect('/admin', 'refresh');
        }
        
        // обработчик - удаление блока
        function delete_block($b_id)
        {
            $this->db->delete('blocs', array('b_id' => $b_id));
			
			//заносим сообщение об успешном удалении блока - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Блок удалён');
            $this->session->set_flashdata($message_data);
			
            redirect('/admin', 'refresh'); 
        }
                
        // обработчик - удаление ряда с блоками внутри (каскадное удаление в mysql)
        function delete_row($r_id)
        {
            $this->db->delete('rows', array('r_id' => $r_id));
			
			//заносим сообщение об успешном удалении ряда - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Строка удалена (row-id:' . $r_id . ')');
            $this->session->set_flashdata($message_data);
			
            redirect('/admin', 'refresh');
        }
        
        // обработчик - удаление фото
        function delete_file()
        {
            $data = array(
                'block_image' => $this->input->post('block_image'),
                'b_id' => $this->input->post('b_id')
            );
            //удалим фото из папки
        	unlink('images/crop/' . $data['block_image']);
            
            //удалим фото из базы данных
            $this->db->where('b_id', $data['b_id']);
            //установим значение столбца пустым
            $this->db->set('image', NULL);
            //и очистим этот столбец
            $this->db->update('blocs');

            //возвращаем результат
            echo 'Delete';
        }
        
        //загрузка файла изображений
        public function do_upload()
        {
                $config['upload_path']          = './images/origin';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))//userfile - имя поля загрузчика картинки
                {             
                        $error = array('error' => $this->upload->display_errors());
                        echo $error;
                }
                else
                {
                        $data = $this->upload->data();
                        echo json_encode($data);
                }
        }
        
        // обрезка изображения
        public function crop_img()
        {
                $data['x'] = $this->input->post('x');
                $data['y'] = $this->input->post('y');
                $data['w'] = $this->input->post('w');
                $data['h'] = $this->input->post('h');
                $data['path'] = $this->input->post('path_file');

                $config['image_library'] = 'gd2';
                $config['source_image'] = 'images/origin/' . $data['path'];
                $config['new_image'] = 'images/crop';
                $config['maintain_ratio'] = FALSE; //пропорции соблюдать не надо так как мы кропим
                
                $config['width']  = $data['w'];
                $config['height'] = $data['h'];
                $config['x_axis'] = $data['x'];
                $config['y_axis'] = $data['y'];
        
                $this->load->library('image_lib', $config);
                $this->image_lib->crop();
                
                //возвращаем json
                echo json_encode($data);

        }
        
        //страница редактирования настроек сайта
        public function settings()
    	{
    	    $this->load->model('Front_model');
    	    // генерация установок сайта
            $settings = $this->Front_model->get_settings();
            //print_r ($settings);
            
            //echo "<pre>";
            //print_r ($data);
            //echo "</pre>";
            
            //если админ - загружаем страничку
            if ($this->ion_auth->is_admin())
            { 
                $this->load->view('admin_tpl/admin_nav');
                $this->load->view('admin_tpl/header');
        		$this->load->view('admin_tpl/settings', $settings);
                $this->load->view('admin_tpl/footer');
            } 
            else  
            {
                redirect('/auth', 'refresh');
            }
    	}
        
        // обработчик - редактирование настроек сайта - запись в базу
        public function settings_post()
    	{
            $data['sitename'] = $this->input->post('sitename');
            $data['email'] = $this->input->post('email');
            $data['phone'] = $this->input->post('tel');
            $data['adress'] = $this->input->post('adress');
            $data['description'] = $this->input->post('description');
            $data['yandex_metrika'] = $this->input->post('yandex_metrika');
            $data['google_analytics'] = $this->input->post('google_analytics');
            
            $this->db->update('settings', $data);
			
			//заносим сообщение об успешном редактировании настроек сайта - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Настройки сайта успешно отредактированы');
            $this->session->set_flashdata($message_data);
			
            redirect('admin/settings', 'refresh');
            
    	}
		
		//форма для создание полей будущей формы обратной связи
        public function create_form()
    	{
            //если админ - загружаем страничку
            if ($this->ion_auth->is_admin())
            {	
				$this->load->view('admin_tpl/admin_nav');
                $this->load->view('admin_tpl/header');
        		$this->load->view('admin_tpl/creator_forms');
                $this->load->view('admin_tpl/footer');
            }
			 else  
            { 
                 redirect('/auth', 'refresh');
            }
    	}
		
		// обработчик - создание поля формы (таблица forms_id) полюс создание полей для этой формы (таблица form_fields)
        public function field_form_db()
    	{
            $data = array(
				'items' => $this->input->post('items'),
            );
			
			$f_data = array(
				'f_form_name' => $this->input->post('f_form_name'),
				'f_form_id' => $this->input->post('f_form_id'),
				'button' => $this->input->post('button'),
            );
            
            foreach($data['items'] as $value){
                $this->db->insert('form_fields', $value);
            }
			
			$this->db->insert('forms_id', $f_data);
            
            //заносим сообщение об успешном создании формы - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Форма <strong>"' . $f_data['f_form_name'] . '"</strong> создана (id: ' . $f_data['f_form_id'] . ').');
            $this->session->set_flashdata($message_data);
			
            redirect('admin/forms', 'refresh');
    	}
		
		// обработчик - редактирование поля формы
        public function edit_form_db()
    	{
			
			$data = array(
				'f_form_name' => $this->input->post('f_form_name'),
				'f_form_id' => $this->input->post('f_form_id'),
				'button' => $this->input->post('button'),
            );
			$this->db->where('f_form_id', $data['f_form_id']);
			$this->db->update('forms_id', $data);
			          
            //заносим сообщение об успешном редактировании формы - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Форма <strong>"' . $data['f_form_name'] . '"</strong> отредактирована (id: ' . $data['f_form_id'] . ').');
            $this->session->set_flashdata($message_data);
            
            redirect($_SERVER['HTTP_REFERER']);
    	}
		
		// обработчик - создание полей для редактируемой формы (страница admin/edit_form/$id_form вкладка полей)
        public function edit_fields_db()
    	{
			
			$data = array(
				'field_type' => $this->input->post('field_type'),
				'form_id' => $this->input->post('form_id_sec'),
				'field_label' => $this->input->post('field_label'),
				'veight' => $this->input->post('veight'),
				'required' => $this->input->post('required'),
				'placeholder' => $this->input->post('placeholder'),
            );
            
            $this->db->insert('form_fields', $data);
			
            //заносим сообщение об успешном добавлении поля - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Поле <strong>' . $data['field_label'] . '</strong> добавлено к форме с <strong>id: ' . $data['form_id'] . '</strong>');
            $this->session->set_flashdata($message_data);
            
            redirect($_SERVER['HTTP_REFERER']);
    	}
		
		// страница всех форм
        public function forms()
    	{
            // выводим массив форм
            $all_forms = $this->Admin_model->get_forms();
			if (empty($all_forms))
			{
					show_404();
			}
			
			//формируем массив (для дальнейшего вывода форм)
            foreach ($all_forms as $l) {
            $data['all_forms'][$l['f_form_id']][] = $l;
            }
			
			//если админ - загружаем страничку
            if ($this->ion_auth->is_admin())
            { 
				$this->load->view('admin_tpl/header');
                $this->load->view('admin_tpl/admin_nav');
        		$this->load->view('admin_tpl/forms_view', $data);
                $this->load->view('admin_tpl/modal_forms/form_ed');
                $this->load->view('admin_tpl/footer');
            }
			else  
            {
                 redirect('/auth', 'refresh');
            }

			//echo "<pre>";
            //print_r ($data['all_forms']);
            //echo "</pre>";
    	}
		
		//удаляем поле формы
        public function delete_form_field($id)
    	{
            $this->db->delete('form_fields', array('field_id' => $id));
            
            //заносим сообщение об успешном удалении поля - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Поле удалено (id: ' . $id . ').');
            $this->session->set_flashdata($message_data);
            redirect($_SERVER['HTTP_REFERER']);
    	}
		
		//обработчик- принимаем данные после редактирования поля формы обратной связи
		public function update_edit_form_field()
    	{
			$data['field_id'] = $this->input->post('field_id');
            $data['field_type'] = $this->input->post('field_type');
            $data['form_id'] = $this->input->post('form_id');
            $data['field_label'] = $this->input->post('field_label');
            $data['veight'] = $this->input->post('veight');
            $data['required'] = $this->input->post('required');
			$data['placeholder'] = $this->input->post('placeholder');
			
            $this->db->where('field_id', $data['field_id']);
			$this->db->update('form_fields', $data);

			//заносим сообщение об успешном редактировании настроек сайта - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Поле отредактировано');
            $this->session->set_flashdata($message_data);
			
			redirect($_SERVER['HTTP_REFERER']);
    	}
		
		// обработчик форм обратной связи
        public function send_form()
    	{
			//общие данные формы помещаем в таблицу correspondence
			$data_f['user_ip'] = $this->input->ip_address();
			$data_f['form_id'] = $this->input->post('form_id');
            
			$this->db->insert('correspondence', $data_f);
			
			//данные полей помещаем в таблицу correspondence_fields
            $post = $this->input->post();
			//удалим из массива значение скрытого поля form_id, мы его использовали выше для записи в таблицу correspondence
			unset($post['form_id']);
            //чистим
            $post = $this->security->xss_clean($post);
			
			//достаём из базы только что сгенерированный id письма - см.выше "$this->db->insert('correspondence', $data_f)" 
			//чтобы в каждом поле поместить его значение, для связи полей письма из одной таблицы с самим письмом из другой таблицы
			$reff_id = $this->db->insert_id();
			
			foreach ($post as $key => $value) {
			$data = array(
               'field_id' => $key ,
               'field_value' => $value ,
               'reff_id' => $reff_id ,
            );
			$this->db->insert('correspondence_fields', $data);
            }
            
            //отправка почты
            $settings = $this->Admin_model->get_settings();
            $this->load->library('email');
            $this->email->from($settings['sitename']);
            $this->email->to($settings['email']);
            $this->email->subject('Вам пришло письмо');
            $this->email->message('Содержимое письма доступно по ссылке ' . base_url() . 'admin/one_correspondence/' . $data['reff_id'] );	
            
            $this->email->send();
            
            echo $this->email->print_debugger();
            
			//заносим сообщение об успешном отправлении формы - в сессию
            $message_data = array('message_type' => "alert alert-success", 'message' => 'Ваше ообщение успешно отправлено');
            $this->session->set_flashdata($message_data);
            
			redirect($_SERVER['HTTP_REFERER']);
			
			echo "<pre>";
            print_r ($settings);
            echo "</pre>";
    	}
		
		// страница всей корреспонденции
        public function correspondence()
    	{
    	    $this->load->library('pagination');
            $config['base_url'] = base_url().'admin/correspondence/';
            //общее количество писем
            $config['total_rows'] = $this->db->count_all_results('correspondence');
            //число писем на странице
            $config['per_page'] = 20;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '<li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $this->pagination->initialize($config);
            
			$data['corresp'] = $this->Admin_model->get_all_corresp($config['per_page'],$this->uri->segment(3));
			
            //если админ - загружаем страничку
            if ($this->ion_auth->is_admin())
            { 
                $this->load->view('admin_tpl/header');
                $this->load->view('admin_tpl/admin_nav');
        		$this->load->view('admin_tpl/all_correspondence', $data);
                $this->load->view('admin_tpl/footer');
            } 
            else  
            { 
                 redirect('/auth', 'refresh');
            }
			
			//echo "<pre>";
            //print_r ($config['total_rows']);
            //echo "</pre>";
    	}
		
		// страница одного письма
        public function one_correspondence($id)
    	{
			$data['corresp'] = $this->Admin_model->get_one_corresp($id);
			if (empty($data['corresp']))
			{
					show_404();
			}
			
            //если админ - загружаем страничку
            if ($this->ion_auth->is_admin())
            { 
                $this->load->view('admin_tpl/header');
                $this->load->view('admin_tpl/admin_nav');
        		$this->load->view('admin_tpl/one_correspondence', $data);
                $this->load->view('admin_tpl/footer');
            } 
            else  
            { 
                 redirect('/auth', 'refresh');
            }
			
			//echo "<pre>";
            //print_r ($data);
            //echo "</pre>";
    	}

}